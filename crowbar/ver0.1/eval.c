/*
*  eval.c 表达式评估
*/

/*
* return: CRB_Value
*/
static CRB_Value
eval_expression(CRB_Interpreter *inter, LocalEnvironment *env, Expression *expr){
    CRB_Value v;
    switch (expr->type) {
        case BOOLEAN_EXPRESSION: //布尔变量
            v = eval_boolean_expression(expr->u.boolean_value);
            break;
        case INT_EXPRESSION:     //整数变量
            v = eval_int_expression(expr->u.int_value);
            break;
        case DOUBLE_EXPRESSION: //实数型变量
            v = eval_double_expression(expr->u.double_value);
            break;
        case STRING_EXPRESSION:  //字符串变量
            v = eval_string_expression(inter, expr->u.string_value);
            break;
        case INDENTIFIER_EXPRESSION: //变量
            v = eval_identifier_expression(inter, env, expr);
            break;
        case ASSIGN_EXPRESSION:  //赋值表达式
            v = eval_assign_expression(inter, env,
                                       expr->u.assgin_expression.variable,
                                       expr->u.assgin_expression.operand);
            break;
        /*
        * 大部分二元运算都整合在eval_binary_expression中
        */
        case ADD_EXPRESSION:
        case SUB_EXPRESSION:
        case MUL_EXPRESSION:
        case DIV_EXPRESSION:
        case MOD_EXPRESSION:
        case EQ_EXPRESSION:
        case NE_EXPRESSION:
        case GT_EXPRESSION:
        case GE_EXPRESSION:
        case LT_EXPRESSION:
        case LE_EXPRESSION:
            v = crb_eval_binary_expression(inter, env, expr->type,
                                           expr->u.binary_expresison.left,
                                           expr->u.binary_expresison.right);
            break;
        case LOGICAL_OR_EXPRESSION:
        case LOGICAL_AND_EXPRESSION:
            v = eval_logical_and_or_expression(inter, env, expr->type,
                                               expr->u.binary_expresison.left,
                                               expr->u.binary_expresison.right);
            break;
        case MINUS_EXPRESSION:
            v = crb_eval_minus_expression(inter, env, expr->u.minus_expression);
            break;
        case FUNCTION_CALL_EXPRESSION:
            v = eval_function_call_expression(inter, env, expr);
            break;
        case NULL_EXPRESSION:
            v = eval_null_expression();
            break;
        case EXPRESSION_TYPE_COUNT_PLUS_1:
        default:
            DBG_panic(("bad case, type %d\n", expr->type));
    }
}

static CRB_Value
eval_int_expression(int int_value){
    CRB_Value v;
    v.type = CRB_INT_VALUE;
    v.u.int_value = int_value;
    return v;
}

/* NOTE: CRB_Value的定义和Expression的好像 */
CRB_Value
crb_eval_binary_expression(CRB_Interpreter *inter, LocalEnvironment *env, ExpressionType operator,
                           Expression *left, Expression *right)
{
    CRB_Value left_val;
    CRB_Value right_val;
    CRB_Value result;
    left_val = eval_expression(inter, env, left);
    right_val = eval_expression(inter, env, right);

    if (left_val.type == CRB_INT_VALUE && right_val.type == CRB_INT_VALUE){
        eval_binary_int(inter, operator, left_val.u.int_value, right_val.u.int_value, &result, left->line_number);
    } else if (left_val.type == CRB_DOUBLE_VALUE && right_val.type == CRB_DOUBLE_VALUE){
        eval_binary_double(inter, operator, left_val.u.double_value, right_val.u.double_value, &result, left->line_number);
    } else if (left_val.type == CRB_INT_VALUE && right_val.type == CRB_DOUBLE_VALUE){
        left_val.u.double_value = left_val.u.int_value;
        eval_binary_double(inter, operator, left_val.u.double_value, right_val.u.double_value, &result, left->line_number);
    } else if (left_val.type == CRB_DOUBLE_VALUE && right_val.type == CRB_INT_VALUE){
        right_val.u.double_value = right_val.u.int_value;
        eval_binary_double(inter, operator, left_val.u.double_value, right_val.u.double_value, &result, left->line_number);
    } else if (left_val.type == CRB_BOOLEAN_VALUE && right_val.type == CRB_BOOLEAN_VALUE){
        result.type = CRB_BOOLEAN_VALUE;
        result.u.boolean_value = eval_binary_boolean(inter, operator, left_val.u.boolean_value, right_val.u.boolean_value,
                                                    &result, left->line_number);
    } else if (left_val.type == CRB_STRING_VALUE && operator == ADD_EXPRESSION){  //字符串加符号
        char buf[LINE_BUF_SIZE];
        CRB_STRING  *right_str;
        if (right_val.type == CRB_INT_VALUE){
            sprintf(buf, "%d", right_val.u.int_value);
            right_str = crb_create_crowbar_string(inter, MEM_strdup(buf));
        } else if (right_val.type == CRB_DOUBLE_VALUE){
            sprintf(buf, "%f", right_val.u.double_value);
            right_str = crb_create_crowbar_string(inter, MEM_strdup(buf));
        } else if (right_val.type == CRB_BOOLEAN_VALUE){
            if (right_val.u.boolean_value){
                right_str = crb_create_crowbar_string(inter, MEM_strdup("true"));
            } else {
                right_str = crb_create_crowbar_string(inter, MEM_strdup("false"));
            }
        } else if (right_val.type == CRB_STRING_VALUE){
            right_str = right_val.u.string_value;  // string_value是CRB_STRING *
        } else if (right_val.type == CRB_NATIVE_POINTER_VALUE){
            sprintf(buf, "%s:%p", right_val.u.native_pointer.info->name, right_val.u.native_pointer.info->pointer);
            right_str = crb_create_crowbar_string(inter, MEM_strdup(buf));
        } else if (right_val.type == CRB_NULL_VALUE){
            right_str = crb_create_crowbar_string(inter, MEM_strdup("null"));
        }
        result.type = CRB_STRING_VALUE;
        result.u.string_value = chain_string(inter, left_val.u.string_value, right_str);
    } else if (left_val.type == CRB_STRING_VALUE && right_val.type == CRB_STRING_VALUE){
        result.type = CRB_BOOLEAN_VALUE;
        result.u.boolean_value = eval_compare_string(operator, &left_val, &right_val, left->line_number);
    } else if (left_val.type == CRB_NULL_VALUE || right_val.type == CRB_NULL_VALUE){
        result.type = CRB_BOOLEAN_VALUE;
        result.u.boolean_value = eval_binary_null(operator, &left_val, &right_val, left->line_number);
    } else {
        char *op_str = crb_get_operator_string(operator);
        crb_runtimr_error(left->line_number, BAD_OPERAND_TYPE_ERR, STRING_MESSAGE_ARGUMENT, "operator", op_str,
                          MESSAGE_ARGUMENT_END);
    }
    return result;
}

static void
eval_binary_int(CRB_Interpreter *inter, ExpressionType operator, int left, int right, CRB_Value *result, int line_number){
    if (dkc_is_match_operator(operator)){ //加减乘除?
        result->type = CRB_INT_VALUE;
    } else if (dkc_is_compare_operator(operator)){
        result->type = CRB_BOOLEAN_VALUE;
    } else {
        DBG_panic(("operator.. %d\n", operator));
    }
    switch (operator) {
        case BOOLEAN_EXPRESSION:
        case INT_EXPRESSION:
        case DOUBLE_EXPRESSION:
        case STRING_EXPRESSION:
        case INDENTIFIER_EXPRESSION:
        case ASSIGN_EXPRESSION:
            DBG_panic(("bad case...%d", operator));
            break;
        case ADD_EXPRESSION:
            result->u.int_value = left + right;
            break;
        case SUB_EXPRESSION:  //subtraction
            result->u.int_value = left - right;
            break;
        case MUL_EXPRESSION:
            result->u.int_value = left * right;
            break;
        case DIV_EXPRESSION:
            result->u.int_value = left / right;
            break;
        case MOD_EXPRESSION:
            result->u.int_value = left % right;
            break;
        case LOGICAL_AND_EXPRESSION:
        case LOGICAL_OR_EXPRESSION:
            DBG_panic(("operator.. %d\n", operator));
            break;
        case EQ_EXPRESSION:
            result->u.boolean_value = left == right;
            break;
        case NE_EXPRESSION:
            result->u.boolean_value = left != right;
            break;
        case GT_EXPRESSION:
            result->u.boolean_value = left > right;
            break;
        case GE_EXPRESSION:
            result->u.boolean_value = left >= right;
            break;
        case LT_EXPRESSION:
            result->u.boolean_value = left < right;
            break;
        case LE_EXPRESSION:
            result->u.boolean_value = left <= right;
            break;
        case MINUS_EXPRESSION:
        case FUNCTION_CALL_EXPRESSION:
        case NULL_EXPRESSION:
        case EXPRESSION_TYPE_COUNT_PLUS_1:
        default:
            DBG_panic(("operator.. %d\n", operator));
    }
}

static CRB_Value
eval_function_call_expression(CRB_Interpreter *inter, LocalEnvironment *env, Expression *expr){
    CRB_Value  value;
    FunctionDefinition  *func;
    char *identifier = expr->u.function_call_expression.identifier;
    func = crb_search_function(identifier);
    if (func == NULL){
        crb_runtimr_error(expr->line_number, FUNCTION_NOT_FOUND_ERR, STRING_MESSAGE_ARGUMENT, "name", identifier,
                          MESSAGE_ARGUMENT_END);
    }
    switch (func->type){
        case CROWBAR_FUNCTION_DEFINITION:
            value = call_crowbar_function(inter, env, expr, func);
            break;
        case NATIVE_FUNCTION_DEFINITION:  // c语言函数(内置函数)
            value = call_native_function(inter, env, expr, func->u.native_f.proc);
            break;
        default:
            DBG_panic(("operator.. %d\n", operator));
    }
    return value;
}

/*
* 调用函数
*/
static CRB_Value
call_crowbar_function(CRB_Interpreter *inter, LocalEnvironment *env, Expression *expr, FunctionDefinition *func){
    CRB_Value  value;
    StatementResult  result;
    ArgumentList  *arg_p;
    ParameterList  *param_p;
    LocalEnvironment  *local_env;
    // 开辟空间存放函数局部变量
    local_env = alloc_local_environment();
    /*
    * 对参数进行评估，并存放到局部变量中
    * arg_p指向实参链，param_p指向形参链
    */
    for (arg_p = expr->u.function_call_expression.argument, param_p = func->u.crowbar_f.parameter;
         arg_p;
         arg_p = arg_p->next, param_p = param_p->next){
        CRB_Value arg_val;
        if (param_p == NULL){
            crb_runtimr_error(expr->line_number, ARGUMENT_TOO_MANY_ERR, MESSAGE_ARGUMENT_END);
        }
        arg_val = eval_expression(inter, env, arg_p->expression);    // 解析实参
        crb_add_local_variable(local_env, param_p->name, &arg_val);  //更新实参
    }
    if (param_p){
        crb_runtimr_error(expr->line_number, ARGUMENT_TOO_FEW_ERR, MESSAGE_ARGUMENT_END);
    }
    // 运行函数内部语句
    // NOTE 有个疑问，怎么解决函数内部调用全局变量
    result = crb_execute_statement_list(inter, local_env, func->u.crowbar_f.block->statement_list);
    if (result.type = RETURN_STATEMENT_RESULT){
        value = result.u.return_value;
    } else {
        value.type = CRB_NULL_VALUE;
    }
    // NOTE dispose处置， 我的英语^_^
    dispose_local_environment(inter, local_env);
    return value;
}
