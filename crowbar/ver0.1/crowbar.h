typedef enum {
    PARSE_ERR = 1,
    CHARACTER_INVALID_ERR,
    FUNCTION_MULTIPLE_DEFINE_ERR,
    COMPILE_ERROR_COUNT_PLUS_1
} CompileError;

typedef enum {
    VARIABLE_NOT_FOUND_ERR = 1,
    FUNCTION_NOT_FOUND_ERR,
    ARGUMENT_TOO_MANY_ERR,
    RUNTIME_ERROR_COUNT_PLUS_1
};

struct Expression_tab {
    ExpressionType type;
    int line_number;
    union {
        CRB_Boolean              boolean_value;
        int                      int_value;
        double                   double_value;
        char                     *string_value;
        char                     *identifier;
        AssignExpression         assgin_expression;
        BinaryExpression         binary_expresison;
        Expression               *minus_expression;  //负号
        FunctionCallExpression   function_call_expression;
    } u;
};

typedef enum {
    BOOLEAN_EXPRESSION = 1,
    INT_EXPRESSION,
    DOUBLE_EXPRESSION,
    STRING_EXPRESSION,            /* 字符串常量 */
    INDENTIFIER_EXPRESSION,       /* 变量 */
    ASSIGN_EXPRESSION,            /* 赋值表达式 */
    ADD_EXPRESSION,
    SUB_EXPRESSION,
    MUL_EXPRESSION,
    DIV_EXPRESSION,
    MOD_EXPRESSION,               /* 求余 */
    EQ_EXPRESSION,                /* == */
    NE_EXPRESSION,                /* != */
    GT_EXPRESSION,                /* > */
    GE_EXPRESSION,                /* >= */
    LT_EXPRESSION,                /* < */
    LE_EXPRESSION,                /* <= */
    LOGICAL_AND_EXPRESSION,
    LOGICAL_OR_EXPRESSION,
    MINUS_EXPRESSION,             /* 单目取负 */
    FUNCTION_CALL_EXPRESSION,     /* 函数调用表达式 */
    NULL_EXPRESSION,
    EXPRESSION_TYPE_COUNT_PLUS_1
} ExpressionType;

// 二元表达式
typedef struct {
    Expression *left;
    Expression *right;
} BinaryExpression;

struct Statement_tag {
    StatementType     type;               /* 像break或者return这样的不需要保存，就没有放在union中 */
    int               line_number;
    union {
        Expression         *expression_s;  /* 表达式语句 */
        GlobalStatement    global_s;       /* global语句 */
        IfStatement        if_s;           /* if语句 */
        WhileStatement     while_s;        /* while语句 */
        ForStatement       for_S;
        ReturnStatement    return_s;
    }
};

typedef struct {
    Expression *condition;
    Block      *block;     /* 可执行块 */
} WhileStatement;

typedef enum {
    EXPRESSION_STATEMENT = 1;
    GLOBAL_STATEMENT,
    IF_STATEMENT,
    WHILE_STATEMENT,
    FOR_STATEMENT,
    RETURN_STATEMENT,
    BREAK_STATEMENT,
    CONTINUE_STATEMENT,
    STATEMENT_TYPE_COUNT_PLUS_1,
} StatementType;

typedef enum {
    NORMAL_STATEMENT_RESULT = 1,
    RETURN_STATEMENT_RESULT,
    BREAK_STATEMENT_RESULT,
    CONTINUE_STATEMENT_RESULT,
    STATEMENT_RESULT_COUNT_PLUS_1
} StatementResultType;

typedef enum {
    CRB_BOOLEAN_VALUE = 1,
    CRB_INT_VALUE,
    CRB_DOUBLE_VALUE,
    CRB_STRING_VALUE,
    CRB_NATIVE_POINTER_VALUE,     //原生指针
    CRB_NULL_VALUE
}

typedef struct {
    CRB_ValueType    type;
    union {
        CRB_Boolean         boolean_value;
        int                 int_value;
        double              double_value;
        CRB_String          *string_value;
        CRB_NativePointer   native_pointer;
    }
}

typedef struct {
    StatementResultType type;
    union {
        CRB_Value       return_value;
    } u;
} StatementResult;

typedef struct GlobalVariableRef_tag {
    Variable *variable;  // 指向全局变量
    struct GlobalVariableRef_tag *next;
}

typedef struct {
    Variable *variable;  //指向局部变量
    GlobalVariableRef *global_variable;
} LocalEnvironment;
