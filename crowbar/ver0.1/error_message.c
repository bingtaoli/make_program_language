MessageFormat crb_compile_error_message_forma[]= {
    {"dummy"},
    {"($(token)附近有语法错误)"},
    {"(错误的字符$(bad_char))"},
    {"(函数名重复$(name))"},
    {"dummy"},
}

MessageFormat crb_runtime_error_message_forma[]= {
    {"dummy"},
    {"(找不到变量$(name))"},
    {"(找不到函数$(name))"},
    {"传递的参数多于函数要求的参数"},
    {"传递的参数少于函数要求的参数"},
    {"条件语句类型必须为boolean型"},
    {"dummy"},
}
