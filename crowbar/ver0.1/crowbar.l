%{
//省略c部分
%}
%start COMMENT STRING_LITERAL_STATE
%%
#leftCurly花括号
<INITIAL>"function"   return FUNCTION
<INITIAL>"if"         return IF;
<INITIAL>"("          return LP;
<INITIAL>")"          return RP;
<INITIAL>"{"          return LC:
<INITIAL>"}"          return RC;
#省略其它符号的定义

#标识符
<INITIAL>[A-Za-z_][A-Za-z0-9]* {
	yylval.identifier = crb_create_identifier(yytext);
	return IDENTIFIER;
}

#数值,整数类型和实数类型分别处理
<INITIAL>([1-9][0-9*]|"0") {
	Expression *expression = crb_alloc_expression(INT_EXPRESSION);
	sscanf(yytext, "%d", &expression->u.int_value);
	yylval.expression = expression;
	return INT_LITERAL;
}
<INITIAL>[0-9+\.[0-9]+] {
	Expression *expression = crb_alloc_expression(DOUBLE_EXPRESSION);
	sscanf(yytest, "%lf", &expression->u.double_value);
	yylval.expression = expression;
	return DOUBLE_LITERAL;
}

#定义字符串对的开始
<INITIAL>\"	{
	crb_open_string_literal();
	BEGIN STRING_LITERAL_STATE;
}
<INITIAL>[ \t] ;
<INITIAL>\n {increment_line_number()};

#定义注释开始的地方
<INITIAL># BEGIN_COMMENT;
#非法字符
<INITIAL>. {
	char buf[LINE_BUF_SIZE];
	if (isprint(yytext[0])){
		buf[0] = yytext[0];
		buf[1] = '\0';
	} else {
		sprintf(buf, "0X%02x", (unsigned char)yytext[0]);
	}
	crb_compile_error(CHARACTER_INVALID_ERR, STRING_MESSAGE_ARGUMENT, "bad_char", buf, MESSAGE_ARGUMENT_END);
}
<COMMENT>\n {
	increate_line_number();
	BEGIN INITIAL;
}
<COMMENT>. ;
<STRING_LITERAL_SATTE>\" {
	Expression *expression = crb_alloc_expression(STRING_EXPRESSION);
	expression->u.string_value = crb_close_string_literal();
	yylval.expression = expression;
	BEGIN INITIAL;
	return STRING_LITERAL;
}
<STRING_LITERAL_STATE>\n {
	crb_add_string_literal('\n');
	increment_line_number();
}
<STRING_LITERAL_STATE>\\\"  crb_add_string_literal('"');
<STRING_LITERAL_STATE>\\n   crb_add_string_literal('\n');
<STRING_LITERAL_STATE>\\t   crb_add_string_literal('\t');
<STRING_LITERAL_STATE>\\\\  crb_add_string_literal('\\');
<STRING_LITERAL_STATE>.     crb_add_string_literal(yytext[0]);
%%
