StatementResult
crb_execute_statement_list(CRB_Interpreter *inter, LocalEnvironment *env, StatementList *list){
    StatementList *pos;
    StatementResult result;
    result.type = NORMAL_STATEMENT_RESULT;
    for (pos = list; pos; pos = pos->next){
        result = execute_statement(inter, env, pos->statement);
        if (result.type != NORMAL_STATEMENT_RESULT){
            goto FUNC_END;
        }
    }
    FUNC_END:
        return result;
}

static StatementResult
execute_statement(CRB_Interpreter *inter, LocalEnvironment *env, Statement *statement){
    StatementResult result;
    result.type = NORMAL_STATEMENT_RESULT;
    switch (statement->type) {
        case EXPRESSION_STATEMENT:
            crb_eval_expression(inter, env, statement->u.expression_s);
            break;
        case GLOBAL_STATEMENT:
            result = execute_global_statement(inter, env, statement);
            break;
        case IF_STATEMENT:
            result = execute_if_statement(inter, env, statement);
            break;
        case WHILE_STATEMENT:
            result = execute_while_statement(inter, env, statement);
            break;
        case STATEMENT_TYPE_COUNT_PLUS_1:
            // error
        default:
            DBG_panic(("bad case...%d", statement->type));
    }
}

static StatementResult
execute_while_statement(CRB_Interpreter *inter, LocalEnvironment *env, Statement *statement){
    StatementResult result;
    CRB_Value       cond;
    for (;;){
        cond = crb_eval_expression(inter, env, statement->u.while_s.condition);
        if (!cond.u.boolean_value){
            break;
        }
        result = crb_execute_statement_list(inter, env, statement->u.while_s.block->statement_list);
        /* break, continue, return的处理 */
        /* 如果是continue只是会中断crb_execute_statement_list的执行 */
        if (result.type == RETURN_STATEMENT_RESULT){
            break;
        } else if (result.type == BREAK_STATEMENT_RESULT) {
            result.type = NORMAL_STATEMENT_RESULT;
            break;
        }
    }
    return result;
}
